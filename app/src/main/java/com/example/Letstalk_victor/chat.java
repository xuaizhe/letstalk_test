package com.example.Letstalk_victor;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


public class chat extends AppCompatActivity {
    private EditText getMessage;
    private Button sendMessage;
    private ImageButton getPicture;
    private ListView chatList;
    private String phoneNumber, Username,Messages,otherPhone;

    private TextView showUsername,showMessage,showTime;

    private FirebaseDatabase myFirebaseDatabase;
    private DatabaseReference myRef,chatRef,check;
    private FirebaseListAdapter myAdapter;
    private Query query;
    private Toolbar myToolbar;
    private String URL="";

    String type_1,type_2,item;


    public void setMessage(final DatabaseReference chatRef){
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Messages = getMessage.getText().toString();
                if(Messages.equals("")){
                    Toast.makeText(getApplicationContext(),"Unable to send with empty text holder",Toast.LENGTH_SHORT).show();
                }else if(!Messages.equals("")){
                    chatMessage cMessage = new chatMessage(Messages,Username,URL,phoneNumber);
                    chatRef.push().setValue(cMessage);
                    Toast.makeText(getApplicationContext(),phoneNumber+" "+Username,Toast.LENGTH_SHORT).show();
                    getMessage.setText("");
                }
            }
        });
        getPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                startActivityForResult(Intent.createChooser(intent,"Complete Action using"),2);

            }
        });
    }
    public void listDataMessage(Query query){
        FirebaseListOptions<chatMessage> options = new FirebaseListOptions.Builder<chatMessage>()
                .setQuery(query,chatMessage.class)
                .setLayout(R.layout.chat_layout).build();

        myAdapter = new FirebaseListAdapter<chatMessage>(options) {
            @Override
            protected void populateView(View v,  chatMessage model, int position) {
                showUsername = v.findViewById(R.id.nameTextView);
                showMessage = v.findViewById(R.id.messageTextView);
                showTime  = v.findViewById(R.id.show_time);
                LinearLayout MyLinear = v.findViewById(R.id.layout_message);
                CardView myLinear = v.findViewById(R.id.myCard);

                Date date = new Date(model.getTimeDate());
                DateFormat simple = new SimpleDateFormat("dd/MM/yy HH:mm aa");
                String getDate = simple.format(date);

                // username is correct as the one who sign in set color and aligment
                if (model.getUsername().equals(Username)) {
                    MyLinear.setGravity(Gravity.END);
                    myLinear.setCardBackgroundColor(Color.rgb(138, 120, 227));

                    showMessage.setText(model.getMessageText());
                    showUsername.setText(model.getUsername().toUpperCase());
                    showTime.setText(getDate);

                    showMessage.setTextColor(Color.WHITE);
                    showMessage.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);

                    showUsername.setTextColor(Color.WHITE);
                    showUsername.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);

                    showTime.setTextColor(Color.WHITE);
                    showTime.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);

                    // username is incorrect as the one who sign in set color and aligment
                }else if(!model.getUsername().equals(Username)) {
                    MyLinear.setGravity(Gravity.START);
                    myLinear.setCardBackgroundColor(Color.WHITE);

                    showMessage.setText(model.getMessageText());
                    showUsername.setText(model.getUsername().toUpperCase());
                    showTime.setText(getDate);

                    showMessage.setTextColor(Color.BLACK);
                    showMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    showUsername.setTextColor(Color.BLACK);
                    showUsername.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    showTime.setTextColor(Color.BLACK);
                    showTime.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);


                }
            }
        };
        chatList.setAdapter(myAdapter);
        myAdapter.startListening();

    }
    public void clearAllChat(){
        check.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds:dataSnapshot.getChildren()){
                    ds.getRef().removeValue();
                    Toast.makeText(getApplicationContext(),ds.getKey(),Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public void changeChatUsername(final DatabaseReference chatRef){
        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    String username = ds.child("username").getValue().toString();
                    if(!Username.equals(username)&&phoneNumber.equals(ds.child("phoneNumber").getValue().toString())){
                        String key = ds.getKey();
                        HashMap<String, Object> result = new HashMap<>();
                        result.put("username",Username);
                        chatRef.child(key).updateChildren(result);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Intent Extra = getIntent();
        otherPhone = Extra.getStringExtra("Other_Phone");

        myFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = myFirebaseDatabase.getReference().child("Users");
        chatRef = myFirebaseDatabase.getReference().child("chats");

        myToolbar =findViewById(R.id.my_toolbar);
        myToolbar.setTitle("Letstalk Enterprise");

        setSupportActionBar(myToolbar);

        sendMessage = findViewById(R.id.send_message);
        getPicture = findViewById(R.id.send_picture);
        getMessage = findViewById(R.id.Message_text);
        chatList = findViewById(R.id.list_message);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null){
            phoneNumber = user.getPhoneNumber();
        }
        type_1 = otherPhone+phoneNumber;
        type_2 = phoneNumber+otherPhone;




        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    if(phoneNumber.equals(ds.child("Phone_number").getValue().toString())){
                        Username = ds.child("Username").getValue().toString();

                    }
                }
                //Toast.makeText(getApplicationContext(),phoneNumber+" "+Username,Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),otherPhone+phoneNumber,Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //listDataMessage(query);
        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild(type_1)){
                    query = chatRef.child(type_1);
                    check = chatRef.child(type_1);
                    setMessage(check);
                    listDataMessage(query);
                    changeChatUsername(check);
                }
                else if(dataSnapshot.hasChild(type_2)){
                    query = chatRef.child(type_2);
                    check = chatRef.child(type_2);
                    setMessage(check);
                    listDataMessage(query);
                    changeChatUsername(check);
                }
                else {
                    query = chatRef.child(type_1);
                    check = chatRef.child(type_1);
                    setMessage(check);
                    listDataMessage(query);
                    changeChatUsername(check);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.backMessage:
                Toast.makeText(getApplicationContext(),"clicked",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(chat.this,navigationTab.class);
                startActivity(i);
                break;
            case R.id.clear_chat:
                clearAllChat();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
