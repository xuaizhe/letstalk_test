package com.example.Letstalk_victor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class settings extends AppCompatActivity {

    private TextView shareing,email,back,signout,sUsername,sAbout;
    private String phoneNumber,Username,About;
    private ImageButton changeAbout;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    public void shareToOtherApp(){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Letstalk");
        String shareAppMessage= "\nLet me recommend you this application\n\n";
        shareAppMessage = shareAppMessage + "https://play.google.com/store/apps/details?id=com.openserverbeta.LetsTalk";
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareAppMessage);
        startActivity(Intent.createChooser(shareIntent, "choose one"));
    }
    public void openEmail(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:xuaizhe@nltvc.com?subject=" + "FeedBack" + "&body=" + "Lets us know your Feedback!");
        intent.setData(data);
        startActivity(intent);
    }
    public void setSignout(){

        FirebaseAuth.getInstance().signOut();
        Intent i = new Intent(settings.this,MainActivity.class);
        startActivity(i);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        email    =    findViewById(R.id.image_email);
        shareing =    findViewById(R.id.image_shareApp);
        signout  =    findViewById(R.id.image_Logout);
        back     =    findViewById(R.id.backView);
        sUsername =   findViewById(R.id.show_username);
        sAbout   =    findViewById(R.id.show_about);
        changeAbout = findViewById(R.id.ImageButton_change);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference().child("Users");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user !=null){
            phoneNumber = user.getPhoneNumber();
            Toast.makeText(getApplicationContext(),phoneNumber,Toast.LENGTH_SHORT).show();
        }


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    String dPhoneNumber = ds.child("Phone_number").getValue().toString();
                    if(phoneNumber.equals(dPhoneNumber)){
                        Username = ds.child("Username").getValue().toString();
                        About = ds.child("About").getValue().toString();
                    }
                }
                sUsername.setText(Username);
                sAbout.setText(About);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        changeAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(settings.this,settingChanger.class);
                startActivity(i);
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openEmail();
            }
        });
        shareing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareToOtherApp();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(settings.this,navigationTab.class);
                startActivity(i);
            }
        });
        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSignout();
                Toast.makeText(getApplicationContext(),"Back to Login",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
