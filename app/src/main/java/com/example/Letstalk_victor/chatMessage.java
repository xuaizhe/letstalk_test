package com.example.Letstalk_victor;

import java.util.Date;

public class chatMessage {
    private String messageText;
    private String username;
    private String phoneNumber;
    private String imageURL;
    private long timeDate;

    public chatMessage(String messageText, String username,String imageURL,String phoneNumber){
        this.messageText = messageText;
        this.username = username;
        this.imageURL = imageURL;
        this.phoneNumber = phoneNumber;
        this.timeDate = new Date().getTime();


    }
    public chatMessage(){

    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageURL(){return imageURL;}

    public void setImageURL(String imageURL){this.imageURL = imageURL;}

    public String getPhoneNumber(){return phoneNumber;}

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(long timeDate) {
        this.timeDate = timeDate;
    }





}
