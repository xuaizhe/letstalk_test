package com.example.Letstalk_victor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class settingChanger extends AppCompatActivity {
    private TextView showUsername,showAbout;
    private EditText setAbout,setUsername;
    private Button changeString;
    private String getUserPhoneNumber,getUsername,getAbout,dPhoneNumber;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private Animation myAnimation;

    public void changeAboutOrUsername(){

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    dPhoneNumber = ds.child("Phone_number").getValue().toString();
                    if(getUserPhoneNumber.equals(dPhoneNumber)){
                        String values = setAbout.getText().toString().trim();
                        String getUsername = setUsername.getText().toString().trim();

                        String key = ds.getKey();
                        HashMap<String, Object> result = new HashMap<>();
                        result.put("Username",getUsername);
                        result.put("About",values);
                        myRef.child(key).updateChildren(result);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_changer);

        showAbout    = findViewById(R.id.show_CurrentAbout);
        showUsername = findViewById(R.id.show_CurrentUsername);

        setAbout     = findViewById(R.id.setAboutText);
        setUsername = findViewById(R.id.change_Username);
        changeString = findViewById(R.id.button_setAbout);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference().child("Users");
        FirebaseUser user  = FirebaseAuth.getInstance().getCurrentUser();

        if(user !=null){
            getUserPhoneNumber = user.getPhoneNumber();
            Toast.makeText(getApplicationContext(),getUserPhoneNumber,Toast.LENGTH_SHORT).show();
        }

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                     dPhoneNumber = ds.child("Phone_number").getValue().toString();
                    if(getUserPhoneNumber.equals(dPhoneNumber)){
                        getUsername = ds.child("Username").getValue().toString();
                        getAbout = ds.child("About").getValue().toString();
                    }
                }
                showUsername.setText(getUsername);
                setUsername.setText(getUsername);
                showAbout.setText(getAbout);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        changeString.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAnimation = new AlphaAnimation(1,0);
                myAnimation.setDuration(500);
                myAnimation.setInterpolator(new LinearInterpolator());
                myAnimation.setRepeatMode(Animation.REVERSE);
                changeString.startAnimation(myAnimation);
                changeAboutOrUsername();

            }
        });
    }
}
