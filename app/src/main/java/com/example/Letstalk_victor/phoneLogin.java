package com.example.Letstalk_victor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

//import com.google.firebase.auth.FirebaseAuth;

public class phoneLogin extends AppCompatActivity {
    public Button bConfirm;
    public Spinner sCountry;
    public EditText eEmail,ePhoneNumber;
    public String username,phoneNumbers,country,emails,approval;
    private String about="hello world!";
    private Animation myAnimation;
    private String mytext;

    public static final String ORGANIZATION_KEY = "Organization";
    public static final String DEPARTMENT_KEY ="Department";
    public static final String USERNAME ="Username";
    public static final String EMAILS ="Emails";
    public static final String COUNTRY  ="Country";
    public static final String PHONE_NUMBER  ="Phone_number";
    public static final String Approval="Approval";
    public static final String About ="About";
    public static final String TAG ="Document";

    public FirebaseDatabase myDatabase = FirebaseDatabase.getInstance();
    DatabaseReference myRef = myDatabase.getReference().child("Users");


    private boolean isValidMobilePhoneNumber(String phone) {
        String phoneMatcher ="^\\+?\\(?[0-9]{1,3}\\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?";
        if(phone.matches(phoneMatcher)){
            return true;
        }else
            return false;
    }
    private boolean isValidEmail(String emails){
        String emailMatcher ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if(emails.matches(emailMatcher)){
            return true;
        }else
            return false;
    }
    public void showAlertText(String myText){
        AlertDialog myAlert = new AlertDialog.Builder(this).create();
        myAlert.setTitle("Opps");
        myAlert.setMessage(myText);
        myAlert.setButton(DialogInterface.BUTTON_POSITIVE, "Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        myAlert.show();

    }

    public void handleData(String emails,String phoneNumbers, String country){
        Intent org = getIntent();
        String Organization = org.getStringExtra("Organization");
        String Department = org.getStringExtra("Department");
        username = phoneNumbers;
        approval = "pending";

        Map<String,Object> dataToSaved = new HashMap<String, Object>();
        dataToSaved.put(ORGANIZATION_KEY,Organization);
        dataToSaved.put(DEPARTMENT_KEY,Department);
        dataToSaved.put(USERNAME,username);
        dataToSaved.put(EMAILS,emails);
        dataToSaved.put(PHONE_NUMBER,phoneNumbers);
        dataToSaved.put(COUNTRY,country);
        dataToSaved.put(About,about);
        dataToSaved.put(Approval,approval);
        myRef.push().setValue(dataToSaved);

        Intent e = new Intent(phoneLogin.this,otpActivity.class);
        e.putExtra("phone_number",phoneNumbers);
        startActivity(e);

        Toast.makeText(phoneLogin.this,emails+" "+phoneNumbers+" "+country,Toast.LENGTH_SHORT).show();
    }
    public void dataBasedHandle(final String emails, final String phoneNumbers, final String country){
        Query query = myRef.orderByChild("Phone_number").equalTo(phoneNumbers);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()){
                    for(DataSnapshot ds : dataSnapshot.getChildren()){
                        if(emails.equals(ds.child("Emails").getValue().toString())){
                            String myUsername = ds.child("Username").getValue().toString();
                            Toast.makeText(getApplicationContext(), myUsername+" Welcome Back", Toast.LENGTH_SHORT).show();
                            Intent e = new Intent(phoneLogin.this,otpActivity.class);
                            e.putExtra("phone_number",phoneNumbers);
                            startActivity(e);
                        }else {
                            Toast.makeText(getApplicationContext(),"Incorrect email type try again",Toast.LENGTH_SHORT).show();
                        }
                    }
                }else{
                    handleData(emails,phoneNumbers,country);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);

        bConfirm     =  findViewById(R.id.button_confirm);
        eEmail    =  findViewById(R.id.edit_email);
        ePhoneNumber =  findViewById(R.id.edit_phoneNumber);
        sCountry     =  findViewById(R.id.spinner_country);

        sCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                switch(arg2) {

                    case 0 :
                        ePhoneNumber.setText("+93");
                        break;
                    case 1 :
                        ePhoneNumber.setText("+355");
                        break;
                    case 2 :
                        ePhoneNumber.setText("+213");
                        break;
                    case 3 :
                        ePhoneNumber.setText("+1-684");
                        break;
                    case 4 :
                        ePhoneNumber.setText("+376");
                        break;
                    case 5 :
                        ePhoneNumber.setText("+244");
                        break;
                    case 6 :
                        ePhoneNumber.setText("+1-264");
                        break;
                    case 7 :
                        ePhoneNumber.setText("+674");
                        break;
                    case 8 :
                        ePhoneNumber.setText("+1-268");
                        break;
                    case 9 :
                        ePhoneNumber.setText("+54");
                        break;
                    case 10 :
                        ePhoneNumber.setText("+374");
                        break;
                    case 11 :
                        ePhoneNumber.setText("+60");
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAnimation = new AlphaAnimation(1, 0);
                myAnimation.setDuration(500);
                myAnimation.setInterpolator(new LinearInterpolator());
                myAnimation.setRepeatMode(Animation.REVERSE);
                bConfirm.startAnimation(myAnimation);

                emails = eEmail.getText().toString();
                phoneNumbers = ePhoneNumber.getText().toString();
                country = sCountry.getSelectedItem().toString();

                if (emails.isEmpty() || phoneNumbers.isEmpty()) {
                    mytext ="Error fill in username or phone number fields";
                    showAlertText(mytext);
                } else if (!emails.isEmpty() || !phoneNumbers.isEmpty()) {
                    if (isValidMobilePhoneNumber(phoneNumbers) && isValidEmail(emails)) {
                        dataBasedHandle(emails, phoneNumbers, country);
                    }
                } else {
                    mytext ="Enter a valid email or phone number";
                    showAlertText(mytext);
                    Toast.makeText(phoneLogin.this, "Enter a valid email or phone number", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}