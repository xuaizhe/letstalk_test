package com.example.Letstalk_victor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

public class pendingTab extends AppCompatActivity {
    private TextView contactSysAdmin;
    private TextView backToRegister;
    private TextView showStatus;
    private DatabaseReference myRef;
    private String statusShow="";

    public void emailSysadmin(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:systemadmin@nltvc.com?subject=" + "FeedBack" + "&body=" + "What Error do you faced?");
        intent.setData(data);
        startActivity(intent);
    }
    public void createNotificationChannel(String statusShow){

        Vibrator v =(Vibrator) this.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(300);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "My Notification";
            String description = "A Notification";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID",name,importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        Uri myAlarm = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"CHANNEL_ID")
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_user_friends)
                .setContentTitle("Approval Status")
                .setContentText("You status is "+statusShow)
                .setColor(Color.BLUE)
                .setSound(myAlarm)
                .setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setVibrate(new long[] {});


        Intent i = new Intent(this, MainActivity.class);
        PendingIntent content = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(content);


        int oneTimeID = (int) SystemClock.uptimeMillis();
        NotificationManagerCompat myManager = NotificationManagerCompat.from(pendingTab.this);
        myManager.notify(oneTimeID,builder.build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_tab);

        contactSysAdmin = findViewById(R.id.emailus);
        backToRegister = findViewById(R.id.backLogin);
        showStatus = findViewById(R.id.showMyStatus);

        Intent getPhone = getIntent();
        final String userPhoneNumber = getPhone.getStringExtra("Phone_number");


        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(pendingTab.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Toast.makeText(getApplicationContext(),newToken,Toast.LENGTH_SHORT).show();
            }
        });
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);



        myRef = FirebaseDatabase.getInstance().getReference().child("Users");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    if(userPhoneNumber.equals(ds.child("Phone_number").getValue().toString())){
                        statusShow = ds.child("Approval").getValue().toString();
                        showStatus.setText(statusShow);
                    }
                }
                if(statusShow.equals("approve")){
                    createNotificationChannel(statusShow);
                    Intent i = new Intent(pendingTab.this,navigationTab.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }else if(statusShow.equals("pending")){
                    createNotificationChannel(statusShow);
                }else if(statusShow.equals("rejected")){
                    createNotificationChannel(statusShow);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        backToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (pendingTab.this,navigationTab.class);
                startActivity(i);
            }
        });
        contactSysAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailSysadmin();
            }
        });
    }
}
