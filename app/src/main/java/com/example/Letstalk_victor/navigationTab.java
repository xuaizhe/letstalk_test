package com.example.Letstalk_victor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;




public class navigationTab extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabItem tab1,tab2,tab3;
    private ImageButton bSettings;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    public PagerAdapter pagerAdapter;

    private String phoneNumber;
    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_tab);


        tabLayout = findViewById(R.id.tabLayout_letstalk);
        tab1 =     findViewById(R.id.tab1);
        tab2 =      findViewById(R.id.tab2);
        tab3 =      findViewById(R.id.tab3);
        bSettings = findViewById(R.id.button_settings);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference().child("Users").child("Register");

        Intent pNumber = getIntent();
        phoneNumber = pNumber.getStringExtra("Phone_number");

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        pagerAdapter = new PageAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if(tab.getPosition() == 0){
                    pagerAdapter.notifyDataSetChanged();
                }else if(tab.getPosition() ==1){
                    pagerAdapter.notifyDataSetChanged();
                }else if(tab.getPosition() == 2){
                    pagerAdapter.notifyDataSetChanged();
                }

            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));



        bSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(navigationTab.this,settings.class);
                Toast.makeText(getApplicationContext(),phoneNumber,Toast.LENGTH_SHORT).show();
                startActivity(i);
            }
        });







    }
}
