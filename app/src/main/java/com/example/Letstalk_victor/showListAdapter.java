package com.example.Letstalk_victor;

public class showListAdapter {
    private String Username;
    private String About;
    private String Department;
    private String Organization;
    private String PhoneNumber;


    public showListAdapter(){

    }
    public showListAdapter(String Username, String About, String Department, String Organization, String PhoneNumber){
        this.Username = Username;
        this.About = About;
        this.Department = Department;
        this.Organization = Organization;
        this.PhoneNumber = PhoneNumber;
    }


    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        this.Username = username;
    }

    public String getAbout() {
        return About;
    }

    public void setAbout(String About) {
        this.About = About;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String Department) {
        this.Department = Department;
    }

    public String getOrganization() {
        return Organization;
    }

    public void setOrganization(String Organization) {
        this.Organization = Organization;
    }

    public String getPhoneNumber(){return PhoneNumber;}

    public void setPhoneNumber(String PhoneNumber){this.PhoneNumber = PhoneNumber;}



}
