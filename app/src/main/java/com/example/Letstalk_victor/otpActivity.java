package com.example.Letstalk_victor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class otpActivity extends AppCompatActivity   {
    public TextView sPhoneNumber,back;
    public EditText editTextCode;
    public Button bConfirm;

    public String mVerificationId,number;
    public FirebaseAuth mAuth;

    private Animation myAnimation;
    public void showPhoneNumber(){
        Intent Extra = getIntent();
        number = Extra.getStringExtra("phone_number");
        sPhoneNumber.setText(number);
        sendVerificationCode(number);

    }
    public void sendVerificationCode(String phoneNumber){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60, TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }
    public PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if(code !=null){
                editTextCode.setText(code);
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(otpActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

        }
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };
    public void verifyVerificationCode(String code){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId,code);

        signInWithPhoneAuthCredential(credential);
    }
    public void signInWithPhoneAuthCredential(PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential).addOnCompleteListener(otpActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser user = task.getResult().getUser();
                    Toast.makeText(getApplicationContext(),"You have sign in succesfully "+user,Toast.LENGTH_SHORT).show();

                    Intent i = new Intent (otpActivity.this,pendingTab.class);
                    i.putExtra("Phone_number",number);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }else{
                    String message = "SomeThing wrong";
                    if(task.getException() instanceof FirebaseAuthInvalidCredentialsException){
                        message ="Invalid code.. entered";
                    }
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
                    snackbar.setAction("Dismiss", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    snackbar.show();

                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        mAuth = FirebaseAuth.getInstance();
        bConfirm = (Button) findViewById(R.id.button_confirm);
        sPhoneNumber = (TextView) findViewById(R.id.showPhoneNumber);
        back =(TextView) findViewById(R.id.back_view);
        editTextCode =(EditText) findViewById(R.id.editTextCode);

        showPhoneNumber();

        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAnimation = new AlphaAnimation(1,0);
                myAnimation.setDuration(500);
                myAnimation.setInterpolator(new LinearInterpolator());
                myAnimation.setRepeatMode(Animation.REVERSE);
                bConfirm.startAnimation(myAnimation);

                String code = editTextCode.getText().toString().trim();
                if(code.isEmpty() || code.length()<6 || code.length() > 6){
                    Toast.makeText(getApplicationContext(),"Code error try again",Toast.LENGTH_SHORT).show();
                    return;
                }
                verifyVerificationCode(code);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(otpActivity.this,phoneLogin.class);
                startActivity(i);
            }
        });
    }
}
