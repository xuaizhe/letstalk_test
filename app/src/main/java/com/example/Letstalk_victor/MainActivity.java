package com.example.Letstalk_victor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public Spinner sOrganization,sDepartment;
    public Button bRegister;
    private String organiation, department;
    private Animation myAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sOrganization = findViewById(R.id.spinner_organization);
        sDepartment = findViewById(R.id.spinner_deparment);
        bRegister = findViewById(R.id.button_register);

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAnimation = new AlphaAnimation(1,0);
                myAnimation.setDuration(500);
                myAnimation.setInterpolator(new LinearInterpolator());
                myAnimation.setRepeatMode(Animation.REVERSE);
                bRegister.startAnimation(myAnimation);

                organiation = sOrganization.getSelectedItem().toString().trim();
                department = sDepartment.getSelectedItem().toString().trim();

                Toast.makeText(getApplicationContext(),organiation+"  "+department,Toast.LENGTH_SHORT).show();
                Intent i = new Intent(MainActivity.this,phoneLogin.class);
                i.putExtra("Organization",organiation);
                i.putExtra("Department",department);
                startActivity(i);
            }
        });
    }
}
