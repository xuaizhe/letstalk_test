package com.example.Letstalk_victor;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;


public class tab1 extends Fragment {
    private RecyclerView myRecView;
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child("Users");
    private FirebaseRecyclerAdapter<showListAdapter, DataViewer> adapter;
    private String Username, About, Department, Organization,phoneNumber;
    private showListAdapter myList;


    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState){

        View v =layoutInflater.inflate(R.layout.fragment_1,container,false);
        myRecView = (RecyclerView) v.findViewById(R.id.myDatabaseView);
        myRecView.setLayoutManager(new LinearLayoutManager(v.getContext()));

        Query query = FirebaseDatabase.getInstance().getReference().child("Users");
        FirebaseRecyclerOptions<showListAdapter> options = new FirebaseRecyclerOptions.Builder<showListAdapter>()
                .setQuery(query, new SnapshotParser<showListAdapter>() {
                    @NonNull
                    @Override
                    public showListAdapter parseSnapshot(@NonNull DataSnapshot snapshot) {

                        Username = snapshot.child("Username").getValue().toString();
                        About = snapshot.child("About").getValue().toString();
                        Department = snapshot.child("Department").getValue().toString();
                        Organization = snapshot.child("Organization").getValue().toString();
                        phoneNumber = snapshot.child("Phone_number").getValue().toString();

                        myList = new showListAdapter(Username,About,Department,Organization,phoneNumber);
                        myList.setUsername(Username);
                        myList.setAbout(About);
                        myList.setDepartment(Department);
                        myList.setOrganization(Organization);
                        myList.setPhoneNumber(phoneNumber);
                        return myList;
                    }
                })
                .build();

        adapter = new FirebaseRecyclerAdapter<showListAdapter, DataViewer>(options) {
            @NonNull
            @Override
            public DataViewer onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_list_layout, parent, false);

                DataViewer dataViewer = new DataViewer(view);
                return dataViewer;
            }

            @Override
            protected void onBindViewHolder(@NonNull final DataViewer holder, final int position, @NonNull final showListAdapter myList) {
                //View myView;
                holder.showUsername.setText(myList.getUsername().toUpperCase());
                holder.showAbout.setText(myList.getAbout());
                holder.showOrganizaiton.setText(myList.getOrganization());
                holder.showDepartment.setText(myList.getDepartment());

                holder.myView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String otherPhone = myList.getPhoneNumber();
                        Intent i = new Intent(v.getContext(),chat.class);
                        i.putExtra("Other_Phone",myList.getPhoneNumber());
                        startActivity(i);
                    }
                });
            }
        };
        myRecView.setAdapter(adapter);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public static class DataViewer extends RecyclerView.ViewHolder  {
        View myView;
        public TextView showUsername, showAbout, showDepartment, showOrganizaiton;

        public DataViewer(View itemViewer) {
            super(itemViewer);
            myView = itemViewer;
            showUsername     = (TextView) myView.findViewById(R.id.dbUsername);
            showAbout        = (TextView) myView.findViewById(R.id.dbAbout);
            showDepartment   = (TextView) myView.findViewById(R.id.dbDepartment);
            showOrganizaiton = (TextView) myView.findViewById(R.id.dbOrganization);
        }
    }
}
